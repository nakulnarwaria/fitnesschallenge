package com.example.dellpc.fitduelfitnesschallenge.constants;

/**
 * Created by Dell Pc on 23-04-2018.
 */

public class ApplicationConstants {
    public static final String EMAIL= "email";
    public static final String PUBLIC_PROFILE= "public_profile";
    public static final String PATTERN_12_HR = "yyyy-MM-dd K:mm a";
    public static final String PATTERN_24_HR = "yyyy-MM-dd hh:mm:ss";

}
