package com.example.dellpc.fitduelfitnesschallenge.challengeRecyclerView;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dellpc.fitduelfitnesschallenge.ProfileStatsChallengesActivity;
import com.example.dellpc.fitduelfitnesschallenge.R;
import com.example.dellpc.fitduelfitnesschallenge.model.ChallengeDetails;
import com.example.dellpc.fitduelfitnesschallenge.statAndUpcomingChallenge.ChallengeFragment;

import java.util.List;
/**
 * Created by Dell Pc on 01-04-2018.
 */

public class ChallengeAdapter extends RecyclerView.Adapter<ChallengeHolder>{
    private List<ChallengeDetails> challenges;
    private Activity activity;
    private String identifier;
    private RecyclerView recyclerView;

    public ChallengeAdapter(List<ChallengeDetails> challenges, Activity activity, String identifier, RecyclerView challengeRecyclerView){
        this.challenges = challenges;
        this.activity = activity;
        this.identifier = identifier;
        this.recyclerView = challengeRecyclerView;
    }

    @Override
    public ChallengeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View view = layoutInflater.inflate(R.layout.challenges_list_overview,parent,false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemPosition = recyclerView.getChildLayoutPosition(view);
                ChallengeFragment.OnFragmentInteractionListener challengeFragmentInteration = (ProfileStatsChallengesActivity)activity;
                challengeFragmentInteration.challengeItemClicked(identifier,challenges.get(itemPosition));
            }
        });
        return new ChallengeHolder(view, parent,activity);
    }

    @Override
    public void onBindViewHolder(ChallengeHolder holder, int position) {
        holder.bind(challenges.get(position));
    }

    @Override
    public int getItemCount() {
        return challenges.size();
    }

}
