package com.example.dellpc.fitduelfitnesschallenge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaCas;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.dellpc.fitduelfitnesschallenge.Utils.ApplicationUtils;
import com.example.dellpc.fitduelfitnesschallenge.constants.ApplicationConstants;
import com.example.dellpc.fitduelfitnesschallenge.service.MyFirebaseMessagingService;
import com.example.dellpc.fitduelfitnesschallenge.service.PedometerService;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;

import org.json.JSONObject;

import java.util.Arrays;

import static com.example.dellpc.fitduelfitnesschallenge.constants.ApplicationConstants.PUBLIC_PROFILE;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "Login Activity";
    private static final int CHALLENGE_ACTIVITY = 1;
    private String email;
    private String password;
    private EditText emailText;
    private EditText passwordText;
    private Button loginButton;
    private LoginButton loginButtonFacebook;
    private Button signUpButton;
    private FirebaseAuth auth;
    private ApplicationUtils applicationUtils;
    private FirebaseAuth.AuthStateListener authStateListener;
    private CallbackManager callbackManager;
    private AccessToken accessToken;
    private FirebaseUser user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Intent msg = new Intent(this,MyFirebaseMessagingService.class);
        startService(msg);
        auth = FirebaseAuth.getInstance();
        emailText = findViewById(R.id.email);
        passwordText = findViewById(R.id.password);
        loginButton = findViewById(R.id.login);
        loginButtonFacebook = findViewById(R.id.login_button);
        signUpButton = findViewById(R.id.signUp);
        applicationUtils = ApplicationUtils.getInstance();
        callbackManager = CallbackManager.Factory.create();
        accessToken = AccessToken.getCurrentAccessToken();
        getUserData();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();
        handleFacebookLogin();


        /*authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(auth.getCurrentUser() != null){
                    //go to second activity
                    user = auth.getCurrentUser();
                    startProfileStatsChallengeActivity(user.getUid(),user.getDisplayName());
                }
            }
        };*/
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleLogin();
            }
        });

        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSignUp();
            }
        });
        enableButtons();
    }

    protected void getUserData(){
        SharedPreferences userData = getSharedPreferences("userData",0);
        emailText.setText(userData.getString("email",""));
        passwordText.setText(userData.getString("password",""));
    }
    @Override
    public void onStart(){
        super.onStart();
        //auth.addAuthStateListener(authStateListener);

    }

    @Override
    public void onResume(){
        super.onResume();
        if(auth!=null && auth.getCurrentUser()!=null){
            startProfileStatsChallengeActivity(auth.getCurrentUser().getUid(),auth.getCurrentUser().getDisplayName());
        }
    }

    protected void handleLogin(){
        if(!validateFields())
            return;

        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){
            applicationUtils.displayToast(LoginActivity.this,getString(R.string.empty));
        }
        disableButtons();
        auth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {

                    Log.d(TAG, "signInWithEmail:success");
                    user = auth.getCurrentUser();
                    startProfileStatsChallengeActivity(user.getUid(),user.getDisplayName());
                } else {
                    enableButtons();
                    Log.w(TAG, "signInWithEmail:failure", task.getException());
                    applicationUtils.displayToast(LoginActivity.this,getString(R.string.authentication_failure));

                }
            }
        });
    }

    protected void handleSignUp(){
        Intent signUpIntent = new Intent(this,SignUpActivity.class);
        startActivity(signUpIntent);
    }

    protected boolean validateFields(){
        email = emailText.getText().toString();
        password = passwordText.getText().toString();

        if(TextUtils.isEmpty(email) || TextUtils.isEmpty(password)){
            applicationUtils.displayToast(LoginActivity.this,getString(R.string.empty));
            return false;
        }
        return true;
    }

    protected void handleFacebookLogin(){

        loginButtonFacebook.setReadPermissions(Arrays.asList(ApplicationConstants.EMAIL,PUBLIC_PROFILE));

        loginButtonFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                enableButtons();
                Log.d(TAG,error.toString());
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void startProfileStatsChallengeActivity(String id, String fullName){
        Intent intent = new Intent(this, ProfileStatsChallengesActivity.class);
        intent.putExtra("id",id);
        intent.putExtra("fullName",fullName);
        startActivityForResult(intent,CHALLENGE_ACTIVITY);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        disableButtons();
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            user = auth.getCurrentUser();
                            if(user!=null){
                                startProfileStatsChallengeActivity(user.getUid(),user.getDisplayName());
                                String userId = user.getUid();
                                String fullname = user.getDisplayName();
                            }

                        } else {
                            enableButtons();
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                        }

                    }
                });
    }

    protected void enableButtons(){
        loginButtonFacebook.setEnabled(true);
        loginButton.setEnabled(true);
        signUpButton.setEnabled(true);
    }

    protected void disableButtons(){
        loginButtonFacebook.setEnabled(false);
        loginButton.setEnabled(false);
        signUpButton.setEnabled(false);
    }

    @Override
    protected void onDestroy(){
        Log.d(TAG," getting called  dsds");
        Intent pedometer = new Intent(this, PedometerService.class);
        stopService(pedometer);
        super.onDestroy();
    }
}