package com.example.dellpc.fitduelfitnesschallenge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.dellpc.fitduelfitnesschallenge.Utils.ApplicationUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class SignUpActivity extends AppCompatActivity {

    private static final String TAG = "SignUpActivity";
    private static final int CHALLENGE_ACTIVITY = 1;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText password;
    private Button signUp;
    private FirebaseAuth auth;
    private ApplicationUtils applicationUtils;
    private FirebaseUser user;
    private SignUpActivity that;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        that = this;

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        email = findViewById(R.id.emailSignUp);
        password = findViewById(R.id.passwordSignUp);
        signUp = findViewById(R.id.signUp);

        auth = FirebaseAuth.getInstance();
        applicationUtils = ApplicationUtils.getInstance();

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleSignUp();
            }
        });
    }

    protected void handleSignUp(){
        if(!validateFields())
            return;

        signUp.setEnabled(false);
        auth.createUserWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            user = auth.getCurrentUser();
                            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                    .setDisplayName(getFullName())
                                    .build();
                            user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        setSharedPreference();
                                        Intent loginIntent = new Intent(that, LoginActivity.class);
                                        NavUtils.navigateUpTo(that,loginIntent);
                                        //navigateUpTo(user.getUid(),user.getDisplayName());
                                    }
                                }
                            });
                            Log.d(TAG, "createUserWithEmail:success");

                        } else {

                            signUp.setEnabled(true);
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            applicationUtils.displayToast(SignUpActivity.this,getString(R.string.authentication_failure));

                        }


                    }
                });
    }


    protected boolean validateFields(){
        String fieldEmail = email.getText().toString();
        String fieldPassword = password.getText().toString();
        String fieldFirstName = firstName.getText().toString();
        String fieldLastName = lastName.getText().toString();

        if(TextUtils.isEmpty(fieldEmail) || TextUtils.isEmpty(fieldPassword)
                || TextUtils.isEmpty(fieldFirstName) || TextUtils.isEmpty(fieldLastName)){
            applicationUtils.displayToast(SignUpActivity.this,getString(R.string.empty));
            return false;
        }
        return true;
    }

    protected String getFullName(){
        return firstName.getText().toString()+" "+lastName.getText().toString();
    }

    protected void setSharedPreference(){
        SharedPreferences userData = getSharedPreferences("userData",0);
        SharedPreferences.Editor profileEditor = userData.edit();
        profileEditor.putString("email",email.getText().toString());
        profileEditor.putString("password",password.getText().toString());
        profileEditor.commit();
    }

}
