package com.example.dellpc.fitduelfitnesschallenge.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.dellpc.fitduelfitnesschallenge.pedometer.StepDetector;
import com.example.dellpc.fitduelfitnesschallenge.pedometer.StepListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Dell Pc on 29-04-2018.
 * AUTHOR THIRD PARTY
 */



public class PedometerService extends IntentService implements SensorEventListener, StepListener {

    private StepDetector simpleStepDetector;
    private SensorManager sensorManager;
    private Sensor accel;
    private long numSteps;
    private double counter;
    private String userId;
    private String challengeId;
    private SharedPreferences sharedPreferencesChallengeDetails;
    private SharedPreferences.Editor challengeDetailsEditor;

    public PedometerService() {
        super("PedometerService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        sharedPreferencesChallengeDetails = getSharedPreferences("challengeDataService",0);
        challengeDetailsEditor =sharedPreferencesChallengeDetails.edit();

        String startTime = intent.getStringExtra("startTime");
        String endTime = intent.getStringExtra("endTime");

        userId = intent.getStringExtra("userId");
        challengeId = intent.getStringExtra("challengeId");
        numSteps = intent.getLongExtra("numSteps",0);

        startPedometer();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
        Date startDate = null;
        Date endDate= null;

        try {
            startDate =  sdf.parse(startTime);
            endDate = sdf.parse(endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        while(System.currentTimeMillis()>startDate.getTime() && System.currentTimeMillis()<endDate.getTime()){
            continue;
        }
        sensorManager.unregisterListener(this);

    }


    protected void startPedometer(){
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        simpleStepDetector = new StepDetector();
        simpleStepDetector.registerListener(this);
        sensorManager.registerListener(PedometerService.this, accel, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    public void step(long timeNs) {
        numSteps++;
        saveStepsToFireBase(numSteps);
    }

    protected void saveStepsToFireBase(long numSteps){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference = database.getReference("challenge_result/"+challengeId);
        DatabaseReference childRef = databaseReference.child(userId+"/steps");
        childRef.setValue(numSteps);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            simpleStepDetector.updateAccel(
                    event.timestamp, event.values[0], event.values[1], event.values[2]);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    public void onDestroy(){
        Log.d("Pedometer Service","getting called onDestry Service");

        sensorManager.unregisterListener(this);
        super.onDestroy();
    }

}
