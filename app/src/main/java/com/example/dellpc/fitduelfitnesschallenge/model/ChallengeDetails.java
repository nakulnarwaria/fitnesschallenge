package com.example.dellpc.fitduelfitnesschallenge.model;

import android.support.annotation.NonNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dell Pc on 31-03-2018.
 */

public class ChallengeDetails implements Comparable<ChallengeDetails>{
    private String id;
    private String endDate;
    private String fee;
    private String maxEntries;
    private String startDate;
    private String name;
    private String totalPrize;
    private String maxFootsteps;
    private String minFootsteps;
    private String steps;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getMaxEntries() {
        return maxEntries;
    }

    public void setMaxEntries(String maxEntries) {
        this.maxEntries = maxEntries;
    }

    public String getStartDate() {
        return startDate;
    }




    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTotalPrize() {
        return totalPrize;
    }

    public void setTotalPrize(String totalPrize) {
        this.totalPrize = totalPrize;
    }

    public String getMaxFootSteps() {
        return maxFootsteps;
    }

    public void setMaxFootSteps(String maxFootSteps) {
        this.maxFootsteps = maxFootSteps;
    }

    public String getMinFootSteps() {
        return minFootsteps;
    }

    public void setMinFootSteps(String minFootSteps) {
        this.minFootsteps = minFootSteps;
    }

    public String getSteps() {
        return steps;
    }

    public void setSteps(String steps) {
        this.steps = steps;
    }


    public int compareTo(ChallengeDetails challengeDetails) {

        String endDate = challengeDetails.getEndDate();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm aa");
        Date passedDate =null;
        Date currentDate=null;
        try {
            passedDate =  sdf.parse(endDate);
            currentDate = sdf.parse(this.endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long current = currentDate.getTime();
        long passed = passedDate.getTime();
        if(current > passed)
           return -1;
        else
            return 1;

    }
}
