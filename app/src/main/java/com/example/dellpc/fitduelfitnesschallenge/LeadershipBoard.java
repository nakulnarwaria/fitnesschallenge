package com.example.dellpc.fitduelfitnesschallenge;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.dellpc.fitduelfitnesschallenge.model.ChallengeUserDetails;
import com.example.dellpc.fitduelfitnesschallenge.model.LeadershipBoardRow;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LeadershipBoard extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ArrayList<ChallengeUserDetails> leadersList;
    private List<LeadershipBoardRow> userList;
    private String challengeId;
    private String userId;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private ListView listView;




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leadership_board);
        challengeId = getIntent().getStringExtra("Challenge Id");
        userId = getIntent().getStringExtra("User Id");
        listView = findViewById(R.id.leadershipBoardList);
        fetchUsers(challengeId);

    }

    private void fetchUsers(String challengeId) {
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("challenge_result"+"/"+challengeId);
        if(myRef!=null) {
            myRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    leadersList=new ArrayList<>();

                    for(DataSnapshot challengeIdSnapshot : dataSnapshot.getChildren()){
                        String user = (String)challengeIdSnapshot.child("name").getValue();
                        String stepsCount = ""+challengeIdSnapshot.child("steps").getValue();
                        int steps = Integer.parseInt(stepsCount);
                        ChallengeUserDetails userInChallenge = new ChallengeUserDetails(steps,user);
                        leadersList.add(userInChallenge);

                    }
                    populateList();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
       // Log.e("Chosen : ",content.get((int)id));

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null){
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return true;
    }

    protected void populateList(){
        int NumberOfUsers = leadersList.size();
        Collections.sort(leadersList);
        int focusedIndex = 0;
        userList = new ArrayList<LeadershipBoardRow>();

        for(ChallengeUserDetails user: leadersList)
        {
            if(user.getUserName()!=null){
                if(user.getUserName().equals(userId))
                    focusedIndex = leadersList.indexOf(user);
            }
            LeadershipBoardRow row = new LeadershipBoardRow(""+(leadersList.indexOf(user)+1),user.getUserName(),""+user.getSteps());
            userList.add(row);

        }

        LeadershipBoardRowAdapter adapter = new LeadershipBoardRowAdapter(this,
                R.layout.leadership_board_row, userList);


        listView.setAdapter(adapter);
        listView.setSelection(focusedIndex);
        listView.requestFocus();

        listView.setOnItemClickListener(this);
    }



}


